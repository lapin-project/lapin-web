# Lapin Web

Web interface for the Lapin download manager

## Setup
```
npm install
```

### Configuration

Edit `config.json`

### Development

To enable hot-reload on change:

```
npm run serve
```
### Production

To build a production-ready version:

```
npm run build
```
