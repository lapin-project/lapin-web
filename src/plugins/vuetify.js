import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import fr from 'vuetify/es5/locale/fr'

Vue.use(Vuetify, {
  theme: {
      primary: '#cddc39',
      secondary: '#795548',
      accent: '#8bc34a',
      error: '#f44336',
      warning: '#ffc107',
      info: '#00bcd4',
      success: '#4caf50'
  },
  iconfont: 'mdi',
  lang: {
    locales: { fr },
    current: 'fr'
  },
})
